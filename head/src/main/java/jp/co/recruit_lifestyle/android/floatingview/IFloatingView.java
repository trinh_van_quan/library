/*
 * ******************************************************************************
 *  Copyright Ⓒ 2016. Dotohsoft.com. All right reversed
 *  Author TrinhQuan. Create on 2016/5/26
 * ******************************************************************************
 */

package jp.co.recruit_lifestyle.android.floatingview;

import android.annotation.TargetApi;
import android.os.Build;
import android.view.ViewTreeObserver;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public interface IFloatingView {

    void setProcessTouchEvent(boolean processTouchEvent);

    void setDraggable(boolean isDraggable);

    float getX();

    float getY();

    int getWidth();

    int getHeight();

    ViewTreeObserver getViewTreeObserver();
}
