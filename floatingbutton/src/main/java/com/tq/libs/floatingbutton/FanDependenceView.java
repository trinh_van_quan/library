/*
 * ******************************************************************************
 *  Copyright Ⓒ 2016. TrinhQuan. All right reversed
 *  Author: TrinhQuan. Created on 2016/5/27
 *  Contact: trinhquan.171093@gmail.com
 * ******************************************************************************
 */

package com.tq.libs.floatingbutton;

import android.content.Context;

public class FanDependenceView extends DependencyView {

    public FanDependenceView(Context context) {
        super(context);
    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }
}
