/*
 * ******************************************************************************
 *  Copyright Ⓒ 2016. TrinhQuan. All right reversed
 *  Author: TrinhQuan. Created on 2016/5/27
 *  Contact: trinhquan.171093@gmail.com
 * ******************************************************************************
 */

package com.tq.libs.floatingbutton;

public interface AssistantManager {

    void setDependencyView(DependencyView view);

    void start();

    void stop();

    void showAssistantButton();

    void showDependency();

}
