/*
 * ******************************************************************************
 *  Copyright Ⓒ 2016. TrinhQuan. All right reversed
 *  Author: TrinhQuan. Created on 2016/5/27
 *  Contact: trinhquan.171093@gmail.com
 * ******************************************************************************
 */

package com.tq.libs.floatingbutton;

import android.content.Context;
import android.view.View;
import android.view.WindowManager;

import jp.co.recruit_lifestyle.android.floatingview.FloatingView;
import jp.co.recruit_lifestyle.android.floatingview.FloatingViewManager;
import jp.co.recruit_lifestyle.android.floatingview.FullscreenObserverView;
import jp.co.recruit_lifestyle.android.floatingview.ScreenChangedListener;

public class AssistantViewManager implements AssistantManager, ScreenChangedListener, FloatingView.FloatingViewUpdateListener {

    public static final int MOVE_DIRECTION_DEFAULT = 0;

    public static final int MOVE_DIRECTION_LEFT = 1;

    public static final int MOVE_DIRECTION_RIGHT = 2;

    public static final int MOVE_DIRECTION_NONE = 3;

    public static final float SHAPE_CIRCLE = 1.0f;

    public static final float SHAPE_RECTANGLE = 1.4142f;

    private final Context mContext;

    private boolean isStarted;

    private final WindowManager mWindowManager;
    private final FloatingViewManager.Options options;

    private AssistantButtonView mAssistantButton;

    private DependencyView mDependencyView;

    private FullscreenObserverView mFullscreenObserverView;

    public AssistantViewManager(Context context, FloatingViewManager.Options options) {
        this.options = options;
        mContext = context;
        mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    }

    private void attachFullScreenObserver() {
        mFullscreenObserverView = new FullscreenObserverView(getContext(), this);
        getWindowManager().addView(mFullscreenObserverView, mFullscreenObserverView.getWindowLayoutParams());
    }

    private void attachAssistantButton() {
        mAssistantButton = new AssistantButtonView(getContext(), this);
        mAssistantButton.setMoveDirection(options.moveDirection);
        mAssistantButton.setOverMargin(options.overMargin);
        mAssistantButton.setShape(options.shape);
        mAssistantButton.setInitCoords(options.floatingViewX, options.floatingViewY);

        getWindowManager().addView(mAssistantButton, mAssistantButton.getWindowLayoutParams());
    }

    private void attachAssistantButtonDependency() {
        if (mDependencyView != null) {
            mDependencyView.setShape(SHAPE_RECTANGLE);
            getWindowManager().addView(mDependencyView, mDependencyView.getWindowLayoutParams());
            mDependencyView.setVisibility(View.GONE);
        }
    }

    private Context getContext() {
        return mContext;
    }

    private WindowManager getWindowManager() {
        return mWindowManager;
    }

    @Override
    public void onScreenChanged(boolean isFullscreen) {

    }

    @Override
    public void setDependencyView(DependencyView view) {
        mDependencyView = view;
    }

    @Override
    public void start() {
        attachFullScreenObserver();
        attachAssistantButton();
        attachAssistantButtonDependency();
        isStarted = true;
    }

    @Override
    public void stop() {
        getWindowManager().removeViewImmediate(mFullscreenObserverView);
        mFullscreenObserverView = null;

        getWindowManager().removeViewImmediate(mAssistantButton);
        getWindowManager().removeViewImmediate(mDependencyView);
        isStarted = false;
    }

    @Override
    public void showAssistantButton() {
        if (!isStarted) {
            throw new IllegalAccessError("assistant bot started");
        }
        hideDependency();
    }

    private void hideAssistantButton() {
        mAssistantButton.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showDependency() {
        if (!isStarted) {
            throw new IllegalAccessError("assistant bot started");
        }
        hideAssistantButton();
        if (mDependencyView != null) {
            mDependencyView.show();
        }
    }

    private void hideDependency() {
        if (mDependencyView != null) {
            mDependencyView.hide();
        }
    }

    @Override
    public void onFloatingButtonMove(int positionX, int positionY) {
        if (mDependencyView != null) {
            mDependencyView.updateAssistantButtonPosition(positionX, positionY);
        }
    }
}
