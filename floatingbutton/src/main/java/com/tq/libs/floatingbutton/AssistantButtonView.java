/*
 * ******************************************************************************
 *  Copyright Ⓒ 2016. TrinhQuan. All right reversed
 *  Author: TrinhQuan. Created on 2016/5/27
 *  Contact: trinhquan.171093@gmail.com
 * ******************************************************************************
 */

package com.tq.libs.floatingbutton;

import android.content.Context;

import jp.co.recruit_lifestyle.android.floatingview.FloatingView;

public class AssistantButtonView extends FloatingView {

    public AssistantButtonView(Context context, FloatingViewUpdateListener listener) {
        super(context);
        setFloatingButtonUpdateListener(listener);
    }
}
