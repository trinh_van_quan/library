/*
 * ******************************************************************************
 *  Copyright Ⓒ 2016. TrinhQuan. All right reversed
 *  Author: TrinhQuan. Created on 2016/5/27
 *  Contact: trinhquan.171093@gmail.com
 * ******************************************************************************
 */

package com.tq.libs.floatingbutton;

import android.content.Context;

import jp.co.recruit_lifestyle.android.floatingview.FloatingView;

public abstract class DependencyView extends FloatingView {

    private int mAssistantPositionX;
    private int mAssistantPositionY;

    public DependencyView(Context context) {
        super(context);
        setProcessTouchEvent(false);
    }

    public abstract void show();

    public abstract void hide();

    public void updateAssistantButtonPosition(int x, int y) {
        mAssistantPositionX = x;
        mAssistantPositionY = y;
    }

    public float getmAssistantPositionX() {
        return mAssistantPositionX;
    }

    public float getmAssistantPositionY() {
        return mAssistantPositionY;
    }
}
