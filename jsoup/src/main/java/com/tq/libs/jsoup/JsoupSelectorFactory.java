/*
 * ******************************************************************************
 *  Copyright Ⓒ 2016. TrinhQuan. All right reversed
 *  Author: TrinhQuan. Created on 2016/6/1
 *  Contact: trinhquan.171093@gmail.com
 * ******************************************************************************
 */

package com.tq.libs.jsoup;

public class JsoupSelectorFactory {

    public static JsoupSelectorExpression newInstance() {
        return JsoupSelectorExpressionImpl.newInstance();
    }
}
