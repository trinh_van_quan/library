/*
 * ******************************************************************************
 *  Copyright Ⓒ 2016. TrinhQuan. All right reversed
 *  Author: TrinhQuan. Created on 2016/6/1
 *  Contact: trinhquan.171093@gmail.com
 * ******************************************************************************
 */

package com.tq.libs.jsoup;

public class MainJsoup {
    public static void main(String[] args) {
        JsoupSelectorExpression expression = JsoupSelectorFactory.newInstance();

        expression.tag("quan").lessThan(3)
                .beginOr().tag("dung").greaterThan(4)
                .endClause();
        System.out.println(expression.get());
        System.out.println(expression.toString());
    }
}
