/*
 * ******************************************************************************
 *  Copyright Ⓒ 2016. TrinhQuan. All right reversed
 *  Author: TrinhQuan. Created on 2016/6/1
 *  Contact: trinhquan.171093@gmail.com
 * ******************************************************************************
 */

package com.tq.libs.jsoup;

abstract class ClauseImpl extends JsoupSelectorExpressionImpl
        implements JsoupSelectorExpression.Clause {

    protected final JsoupSelectorExpression expression;

    protected ClauseImpl(JsoupSelectorExpression expression) {
        super();
        this.expression = expression;
    }

    public JsoupSelectorExpression endClause() {
        joinCommand();
        return expression;
    }

    protected abstract void joinCommand();

    static class PrefixClause extends ClauseImpl {

        private final String prefix;

        public PrefixClause(JsoupSelectorExpression expression, String prefix) {
            super(expression);
            this.prefix = prefix;
        }

        @Override
        protected void joinCommand() {
            if (expression.get().length() == 0) {
                expression.append(get());
            } else {
                expression.append(prefix).append(get());
            }
        }

        public static PrefixClause newInstance(JsoupSelectorExpression expression, String prefix) {
            return new PrefixClause(expression, prefix);
        }
    }
}
