/*
 * ******************************************************************************
 *  Copyright Ⓒ 2016. TrinhQuan. All right reversed
 *  Author: TrinhQuan. Created on 2016/6/1
 *  Contact: trinhquan.171093@gmail.com
 * ******************************************************************************
 */

package com.tq.libs.jsoup;

public interface JsoupSelectorExpression {

    JsoupSelectorExpression tag(String tag);

    JsoupSelectorExpression tagWithNameSpace(String tag, String ns);

    JsoupSelectorExpression withID(String id);

    JsoupSelectorExpression withClass(String clazz);

    JsoupSelectorExpression withAttribute(String attribute);

    JsoupSelectorExpression withAttribute(String attribute, String value);

    JsoupSelectorExpression withAttributeAndStringValue(String attribute, String value);

    JsoupSelectorExpression withAttributePrefix(String prefix);

    JsoupSelectorExpression withAttributeAndValuePrefix(String attribute, String prefix);

    JsoupSelectorExpression withAttributeAndValueSuffix(String attribute, String suffix);

    JsoupSelectorExpression withAttributeAndContainValue(String attribute, String value);

    JsoupSelectorExpression withAttributeAndContainValueWithRegex(String attribute, String regex);

    void root();

    void empty();

    JsoupSelectorExpression firstChild();

    JsoupSelectorExpression lastChild();

    JsoupSelectorExpression firstOfType();

    JsoupSelectorExpression lastOfType();

    JsoupSelectorExpression lessThan(int index);

    JsoupSelectorExpression greaterThan(int index);

    JsoupSelectorExpression equalTo(int index);

    JsoupSelectorExpression has(String selector);

    JsoupSelectorExpression not(String selector);

    JsoupSelectorExpression contains(String text);

    JsoupSelectorExpression matches(String regex);

    JsoupSelectorExpression containsOwn(String text);

    JsoupSelectorExpression matchesOwn(String regex);

    // clause command
    Clause beginOr();

    Clause beginDecentChild();

    Clause beginDirectChild();

    JsoupSelectorExpression endClause();

    JsoupSelectorExpression append(String s);

    String get();

    interface Clause extends JsoupSelectorExpression {

    }
}
