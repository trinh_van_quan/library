/*
 * ******************************************************************************
 *  Copyright Ⓒ 2016. TrinhQuan. All right reversed
 *  Author: TrinhQuan. Created on 2016/6/1
 *  Contact: trinhquan.171093@gmail.com
 * ******************************************************************************
 */

package com.tq.libs.jsoup;

class JsoupSelectorExpressionImpl implements JsoupSelectorExpression {

    protected StringBuilder internalBuilder;

    public JsoupSelectorExpressionImpl() {
        internalBuilder = new StringBuilder();
    }

    public static JsoupSelectorExpression newInstance() {
        return new JsoupSelectorExpressionImpl();
    }

    @Override
    public JsoupSelectorExpression tag(String tag) {
        String tagTrim = tag.trim();
        if (tagTrim.length() != 0) {
            return append(tagTrim);
        }
        return this;
    }

    @Override
    public JsoupSelectorExpression tagWithNameSpace(String tag, String ns) {
        String nsTrim = ns.trim();
        if (nsTrim.length() == 0) {
            return tag(tag);
        } else {
            String tagTrim = tag.trim();
            if (tagTrim.length() != 0) {
                return append(nsTrim).append("|").append(tagTrim);
            }
        }
        return this;
    }

    @Override
    public JsoupSelectorExpression withID(String id) {
        String idTrim = id.trim();
        if (idTrim.length() != 0) {
            return append("#").append(idTrim);
        }
        return this;
    }

    @Override
    public JsoupSelectorExpression withClass(String clazz) {
        String classTrim = clazz.trim();
        if (classTrim.length() != 0) {
            return append(".").append(classTrim);
        }
        return this;
    }

    @Override
    public JsoupSelectorExpression withAttribute(String attribute) {
        String attributeTrim = attribute.trim();
        if (attributeTrim.length() != 0) {
            return append("[").append(attributeTrim).append("]");
        }
        return this;
    }

    @Override
    public JsoupSelectorExpression withAttribute(String attribute, String value) {
        String valueTrim = value.trim();
        if (valueTrim.length() == 0) {
            return withAttribute(attribute);
        } else {
            String attributeTrim = attribute.trim();
            if (attributeTrim.length() != 0) {
                return append("[").append(attributeTrim)
                        .append("=").append(valueTrim).append("]");
            }
        }
        return this;
    }

    @Override
    public JsoupSelectorExpression withAttributeAndStringValue(String attribute, String value) {
        String valueTrim = value.trim();
        if (valueTrim.length() == 0) {
            return withAttribute(attribute);
        } else {
            String attributeTrim = attribute.trim();
            if (attributeTrim.length() != 0) {
                return append("[").append(attributeTrim)
                        .append("=\"").append(valueTrim).append("\"]");
            }
        }
        return this;
    }

    @Override
    public JsoupSelectorExpression withAttributePrefix(String prefix) {
        String prefixTrim = prefix.trim();
        if (prefixTrim.length() != 0) {
            return append("[^").append(prefixTrim).append("]");
        }
        return this;
    }

    @Override
    public JsoupSelectorExpression withAttributeAndValuePrefix(String attribute, String prefix) {
        String prefixTrim = prefix.trim();
        if (prefixTrim.length() == 0) {
            return withAttribute(attribute);
        } else {
            String attributeTrim = attribute.trim();
            if (attributeTrim.length() != 0) {
                return append("[").append(attributeTrim)
                        .append("^=").append(prefixTrim).append("]");
            }
        }
        return this;
    }

    @Override
    public JsoupSelectorExpression withAttributeAndValueSuffix(String attribute, String suffix) {
        String suffixTrim = suffix.trim();
        if (suffixTrim.length() == 0) {
            return withAttribute(attribute);
        } else {
            String attributeTrim = attribute.trim();
            if (attributeTrim.length() != 0) {
                return append("[").append(attributeTrim)
                        .append("$=").append(suffixTrim).append("]");
            }
        }
        return this;
    }

    @Override
    public JsoupSelectorExpression withAttributeAndContainValue(String attribute, String value) {
        String valueTrim = value.trim();
        if (valueTrim.length() == 0) {
            return withAttribute(attribute);
        } else {
            String attributeTrim = attribute.trim();
            if (attribute.trim().length() != 0) {
                return append("[").append(attributeTrim)
                        .append("*=").append(valueTrim).append("]");
            }
        }
        return this;
    }

    @Override
    public JsoupSelectorExpression withAttributeAndContainValueWithRegex(String attribute, String regex) {
        String regexTrim = regex.trim();
        if (regexTrim.length() == 0) {
            return withAttribute(attribute);
        } else {
            String attributeTrim = attribute.trim();
            if (attributeTrim.length() != 0) {
                return append("[").append(attributeTrim)
                        .append("~=").append(regexTrim).append("]");
            }
        }
        return this;
    }

    @Override
    public void root() {
        internalBuilder = new StringBuilder(":root");
    }

    @Override
    public void empty() {
        internalBuilder = new StringBuilder(":empty");
    }

    @Override
    public JsoupSelectorExpression firstChild() {
        return appendTail(":first-child");
    }

    @Override
    public JsoupSelectorExpression lastChild() {
        return appendTail(":last-child");
    }

    @Override
    public JsoupSelectorExpression firstOfType() {
        return appendTail(":first-of-type");
    }

    @Override
    public JsoupSelectorExpression lastOfType() {
        return appendTail(":last-of-type");
    }

    private JsoupSelectorExpression appendTail(String tail) {
        if (get().length() != 0) {
            return append(tail);
        }
        return this;
    }

    @Override
    public JsoupSelectorExpression append(String s) {
        internalBuilder.append(s);
        return this;
    }

    //command
    private JsoupSelectorExpression compound(String command, String value) {
        String commandTrim = command.trim();
        String valueTrim = value.trim();
        if (commandTrim.length() != 0 && valueTrim.length() != 0) {
            if (get().length() != 0) {
                return append(":").append(commandTrim)
                        .append("(").append(valueTrim).append(")");
            }
        }
        return this;
    }

    @Override
    public JsoupSelectorExpression lessThan(int index) {
        return compound("lt", index + "");
    }

    @Override
    public JsoupSelectorExpression greaterThan(int index) {
        return compound("gt", index + "");
    }

    @Override
    public JsoupSelectorExpression equalTo(int index) {
        return compound("eq", index + "");
    }

    @Override
    public JsoupSelectorExpression has(String selector) {
        return compound("has", selector);
    }

    @Override
    public JsoupSelectorExpression not(String selector) {
        return compound("not", selector);
    }

    @Override
    public JsoupSelectorExpression contains(String text) {
        return compound("contains", text);
    }

    @Override
    public JsoupSelectorExpression matches(String regex) {
        return compound("matches", regex);
    }

    @Override
    public JsoupSelectorExpression containsOwn(String text) {
        return compound("containsOwn", text);
    }

    @Override
    public JsoupSelectorExpression matchesOwn(String regex) {
        return compound("matchesOwn", regex);
    }

    // clause command
    @Override
    public Clause beginOr() {
        return ClauseImpl.PrefixClause.newInstance(this, ", ");
    }

    @Override
    public Clause beginDecentChild() {
        return ClauseImpl.PrefixClause.newInstance(this, " ");
    }

    @Override
    public Clause beginDirectChild() {
        return ClauseImpl.PrefixClause.newInstance(this, " > ");
    }

    @Override
    public JsoupSelectorExpression endClause() {
        return this;
    }

    @Override
    public String get() {
        return internalBuilder.toString().trim();
    }

    @Override
    public String toString() {
        return get();
    }
}
