/*
 * Copyright Ⓒ 2016. TrinhQuan. All right reversed
 * Author: TrinhQuan. Created on 2016/3/26
 * Contact: trinhquan.171093@gmail.com
 */

package com.tq.app.libs.data.tuples;

public class Quintet<FIRST, SECOND, THIRD, FORTH, FIFTH> extends Quartet<FIRST, SECOND, THIRD, FORTH> {

    private final FIFTH obj;

    public Quintet(FIRST obj, SECOND second, THIRD third, FORTH forth, FIFTH fifth) {
        super(obj, second, third, forth);
        this.obj = fifth;
    }

    public FIFTH get4(){
        return obj;
    }

}
