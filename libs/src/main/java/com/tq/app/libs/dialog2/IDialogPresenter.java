/*
 * ******************************************************************************
 *  Copyright Ⓒ 2016. Dotohsoft.com. All right reversed
 *  Author TrinhQuan. Create on 2016/5/14
 * ******************************************************************************
 */

package com.tq.app.libs.dialog2;

import com.tq.app.libs.callback.OnDialogCallback;

public interface IDialogPresenter {
    void setDialogCallback(OnDialogCallback dialogCallback);
}
