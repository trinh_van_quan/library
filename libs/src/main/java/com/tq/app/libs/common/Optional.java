/*
 * ******************************************************************************
 *  Copyright Ⓒ 2016. Dotohsoft.com. All right reversed
 *  Author TrinhQuan. Create on 2016/7/29
 * ******************************************************************************
 */

package com.tq.app.libs.common;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.Closeable;
import java.io.IOException;

public class Optional<T> {

    private static final Optional<?> EMPTY = new Optional<>(null);

    private final T obj;

    private Optional(T obj) {
        this.obj = obj;
    }

    public T get() {
        return obj;
    }

    public T get(T defaultValue) {
        return orElse(defaultValue);
    }

    public boolean isPresent() {
        return obj != null;
    }

    public T orElse() {
        return orElse(null);
    }

    public T orElse(T otherValue) {
        if (isPresent()) {
            return obj;
        }
        return otherValue;
    }

    public T orThrow() throws Throwable {
        if (isPresent()) {
            return obj;
        }
        throw new Throwable("Object is not present");
    }

    public Optional<T> filter(@NonNull Predicate<T> predicate) {
        if (!isPresent()) {
            return this;
        }
        if (predicate.test(obj)) return this;
        return empty();
    }

    public <U> Optional<U> map(@NonNull Function<T, U> mapper) {
        if (!isPresent()) {
            return empty();
        }
        return Optional.ofNullable(mapper.apply(obj));
    }

    public <U> Optional<U> chain(@NonNull Function<T, U> chain) {
        return map(chain);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    public static <T> Optional<T> of(T obj) {
        return new Optional<>(obj);
    }

    public static <T> Optional<T> ofNullable(T obj) {
        if (obj == null) return empty();
        return of(obj);
    }

    @SuppressWarnings("unchecked")
    public static <T> Optional<T> empty() {
        return (Optional<T>) EMPTY;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    public static <T> void invoke(@Nullable T obj, @NonNull NonNullableInvoke<T> nonNullableInvoke) {
        if (obj != null) {
            nonNullableInvoke.invoke(obj);
        }
    }

    public static <T, U> Optional<U> get(@Nullable T obj,
                                         @NonNull NonNullableGet<T, U> nonNullableGet) {
        U result = null;
        if (obj != null) {
            result = nonNullableGet.get(obj);
        }
        return Optional.ofNullable(result);
    }

    public static <T extends Closeable> void resources(@Nullable T resource,
                                                       @NonNull NonNullableInvoke<T> nonNullableInvoke) {
        if (resource == null) {
            return;
        }
        try {
            nonNullableInvoke.invoke(resource);
        } finally {
            try {
                resource.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static <T extends Closeable, U> Optional<U> resources(@Nullable T resource,
                                                                 @NonNull NonNullableGet<T, U> nonNullableGet) {
        if (resource == null) {
            return empty();
        }
        try {
            return get(resource, nonNullableGet);
        } finally {
            try {
                resource.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public interface Function<T, U> {
        U apply(T obj);
    }

    public interface Predicate<T> {
        boolean test(T obj);
    }

    public interface NonNullableGet<T, U> {
        U get(T obj);
    }

    public interface NonNullableInvoke<T> {
        void invoke(T obj);
    }
}
