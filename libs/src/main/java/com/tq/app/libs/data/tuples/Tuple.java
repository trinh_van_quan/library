/*
 * Copyright Ⓒ 2016. TrinhQuan. All right reversed
 * Author: TrinhQuan. Created on 2016/3/26
 * Contact: trinhquan.171093@gmail.com
 */

package com.tq.app.libs.data.tuples;

public class Tuple<FIRST> {

    private final FIRST obj;

    public Tuple(final FIRST first) {
        this.obj = first;
    }

    public FIRST get0() {
        return obj;
    }

    public <T> Pair<FIRST, T> addSecond(T obj) {
        return Tuple.getTuple(get0(), obj);
    }

    public static <FIRST> Tuple<FIRST> getTuple(FIRST obj) {
        return new Tuple<>(obj);
    }

    public static <FIRST, SECOND> Pair<FIRST, SECOND> getTuple(FIRST first, SECOND second) {
        return new Pair<>(first, second);
    }

    public static <FIRST, SECOND, THIRD>
    Triplet<FIRST, SECOND, THIRD> getTuple(FIRST first, SECOND second, THIRD third) {
        return new Triplet<>(first, second, third);
    }

    public static <FIRST, SECOND, THIRD, FORTH>
    Quartet<FIRST, SECOND, THIRD, FORTH> getTuple(FIRST first, SECOND second, THIRD third, FORTH forth) {
        return new Quartet<>(first, second, third, forth);
    }

    public static <FIRST, SECOND, THIRD, FORTH, FIFTH>
    Quintet<FIRST, SECOND, THIRD, FORTH, FIFTH>
    getTuple(FIRST first, SECOND second, THIRD third, FORTH forth, FIFTH fifth) {
        return new Quintet<>(first, second, third, forth, fifth);
    }
}
