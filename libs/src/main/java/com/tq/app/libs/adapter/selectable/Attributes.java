/*
 * Copyright Ⓒ 2016. TrinhQuan. All right reversed
 * Author: TrinhQuan. Created on 2016/3/26
 * Contact: trinhquan.171093@gmail.com
 */

package com.tq.app.libs.adapter.selectable;

public abstract class Attributes {
    public enum Mode {
        SINGLE,
        MULTIPLE
    }

    public enum State {
        SELECTABLE,
        PRESSABLE
    }
}
