/*
 * ******************************************************************************
 *  Copyright Ⓒ 2016. Dotohsoft.com. All right reversed
 *  Author TrinhQuan. Create on 2016/7/29
 * ******************************************************************************
 */

package com.tq.app.libs.common;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {

    private static String DEFAULT_PREF = "";

    public static void setDefaultPref(String prefName) {
        if (prefName == null) {
            prefName = "";
        }
        DEFAULT_PREF = prefName;
    }

    public static String getDefaultPref() {
        return DEFAULT_PREF;
    }

    public static SharedPreferences preferences(Context context, String prefName) {
        return context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
    }

    public static SharedPreferences defaultPref(Context context) {
        return preferences(context, DEFAULT_PREF);
    }

    public static void preferenceForEdit(Context context, String prefName, EditorHandler editorHandler) {
        edit(preferences(context, prefName), editorHandler);
    }

    public static void defaultForEdit(Context context, EditorHandler editorHandler) {
        preferenceForEdit(context, DEFAULT_PREF, editorHandler);
    }

    public static void edit(SharedPreferences preferences, EditorHandler editorHandler) {
        SharedPreferences.Editor editor = preferences.edit();
        editorHandler.edit(editor);
        editor.apply();
    }

    public interface EditorHandler {
        void edit(SharedPreferences.Editor editor);
    }
}
