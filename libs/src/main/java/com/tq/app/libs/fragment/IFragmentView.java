/*
 * Copyright Ⓒ 2016. TrinhQuan. All right reversed
 * Author: TrinhQuan. Created on 2016/3/26
 * Contact: trinhquan.171093@gmail.com
 */

package com.tq.app.libs.fragment;

import com.tq.app.libs.mvp.IView;

public interface IFragmentView extends IView {
    String getFragmentID();
}
