/*
 * Copyright Ⓒ 2016. TrinhQuan. All right reversed
 * Author: TrinhQuan. Created on 2016/3/26
 * Contact: trinhquan.171093@gmail.com
 */

package com.tq.app.libs.common;

import com.google.common.base.Joiner;

public class StringUtils {

    public static String join(final String joiner, final Object firstObject, final Object secondObject,
                              final Object... objects) {
        return Joiner.on(joiner).skipNulls().join(firstObject, secondObject, objects);
    }

    public static boolean equal(String lhs, String rhs) {
        return lhs == null && rhs == null || lhs != null && rhs != null && lhs.equals(rhs);
    }

    public static boolean equalIgnoreCase(String lhs, String rhs) {
        return lhs == null && rhs == null || lhs != null && rhs != null && lhs.equalsIgnoreCase(rhs);
    }

    public static boolean empty(String s) {
        return s == null || s.length() == 0;
    }
}
