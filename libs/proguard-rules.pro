# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/TrinhQuan/Library/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# eventbus
-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

#db flow
-keep class com.raizlabs.android.dbflow.config.GeneratedDatabaseHolder

# retrofit
-dontwarn retrofit2.**
#-keep class retrofit2.** { *; }
#-keepattributes Signature
#-keepattributes Exceptions

# jackson
# if you dont use jackson, you can simply using -dontwarn line for jackson below
-dontwarn java.beans.**
-dontwarn org.w3c.dom.bootstrap.**

#-keepattributes *Annotation*,EnclosingMethod,Signature
#-keepnames class com.fasterxml.jackson.** { *; }
#-dontwarn com.fasterxml.jackson.databind.**
#-keep class org.codehaus.** { *; }
#-keepclassmembers public final enum org.codehaus.jackson.annotate.JsonAutoDetect$Visibility {
#    public static final org.codehaus.jackson.annotate.JsonAutoDetect$Visibility *;
#}

# okhttp
#-keep class java.nio.file.** { *; }
-dontwarn java.nio.file.**

# butter knife
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewBinder { *; }
-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}
-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}

# Parcel library
-keep class **$$Parcelable { *; }